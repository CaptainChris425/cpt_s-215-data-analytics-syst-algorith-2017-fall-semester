# -*- coding: utf-8 -*-

import timeit
import numpy as np
import random as rand
import pandas as pd

import matplotlib.pyplot as plt

class Node:
    '''
    '''
    def __init__(self,data):
        self.data = data
        self.next = None
        self.prev = None
    def __str__(self):
        return str(self.data)
    def get_data(self):
        return self.data
    def get_next(self):
        return self.next
    def get_prev(self):
        return self.prev
    def set_next(self, newnext):
        self.next = newnext
    def set_prev(self, newprev):
        self.prev = newprev
    def set_data(self, newdata):
        self.data = newdata

class DLCList:
    def __init__(self):
        self.head = Node(None)
        self.head.set_next(self.head)
        self.head.set_prev(self.head)
        
    def clear(self):
        self.head = Node(None)
        self.head.set_next(self.head)
        self.head.set_prev(self.head)
        
    def __str__(self):
        if self.head.get_data() is not None:
            list_str = "{head}->"
            curr = self.head
            cont = True
            while cont:
                list_str += str(curr)
                list_str += "->"
                curr = curr.get_next()
                if curr is self.head:
                    cont = False
            list_str += "{head}"
            return list_str
        return None
    def add(self, item):
        if self.head.get_data() is not None:
            temp = Node(item)
            self.head.get_prev().set_next(temp)
            temp.set_prev(self.head.get_prev())
            temp.set_next(self.head)
            self.head.set_prev(temp)
            self.head = temp
        else: 
            self.head = Node(item)
            self.head.set_next(self.head)
            self.head.set_prev(self.head)
    def append(self, item):
        if self.head.get_data() is not None:
            temp = Node(item)
            self.head.get_prev().set_next(temp)
            temp.set_prev(self.head.get_prev())
            temp.set_next(self.head)
            self.head.set_prev(temp)
        else: 
            self.head = Node(item)
            self.head.set_next(self.head)
            self.head.set_prev(self.head)
    def insert(self, index, item):
        if self.head.get_data() is not None:
            temp = Node(item)
            if index == 0:
                self.add(item)
            else:
                curr = self.head
                i = 0
                while curr.get_next() is not self.head and i < index-1:
                    curr = curr.get_next()
                    i += 1
                curr.get_next().set_prev(temp)
                temp.set_next(curr.get_next())
                temp.set_prev(curr)
                curr.set_next(temp)
        else: 
            self.head = Node(item)
            self.head.set_next(self.head)
            self.head.set_prev(self.head)
    def pop(self, index):
        if self.head.get_data() is not None or index > self.size():
            if index == -1:
                temp = self.head.get_prev() 
                temp.get_prev().set_next(self.head)
                self.head.set_prev(temp.get_prev())
                return temp
            if index == 0:
                temp = self.head
                temp.get_prev().set_next(temp.get_next())
                temp.get_next().set_prev(temp.get_prev())
                self.head = temp.get_next()
                return temp
            else:
                curr = self.head
                i = 0
                while curr.get_next() is not self.head and i < index -1:
                    curr = curr.get_next()
                    i += 1
                temp = curr.get_next()
                curr.set_next(temp.get_next())
                temp.get_next().set_prev(temp.get_prev)
                return temp
        else: return None
    def remove(self, item):
        if self.head.get_data() is not None:
            curr = self.head
            found = False
            while not found:
                if curr.get_data() == item:
                    found = True
                elif curr.get_next() == self.head:
                    return
                curr = curr.get_next()
            curr = curr.get_prev()
            if curr is self.head :
                self.head = curr.get_next()
            curr.get_prev().set_next(curr.get_next())
            curr.get_next().set_prev(curr.get_prev())
        else: return
        
    def search(self, item):
        curr = self.head
        while True:
            if curr.get_data() == item:
                return True
            if curr.get_next() == self.head:
                return False
            curr = curr.get_next()
    
    def is_empty(self):
        return self.head.get_data() == None
    
    def size(self):
        curr = self.head
        i = 0
        while curr.get_next() is not self.head:
            curr = curr.get_next()
            i += 1
        return i + 1
    def get_indicies(self):
        index = []
        curr = self.head
        i = 0
        while i < self.size():
            index.append((i, curr.get_data()))
            curr = curr.get_next()
            i += 1
        return index
    def see_head(self):
        head = "{head}->"
        head += str(self.head)
        return head
    def is_circular(self):
        '''
        Checks if the list is indeecd circular by itterating twice through
        the list forwards and once backwards and by stopping on the head
        confirming the list is circular
        '''
        i = 0
        curr = self.head
        size = self.size()
        list_str = "{head}->"
        while i < 2*size:
            list_str += str(curr)
            list_str += "->"
            curr = curr.get_next()
            i += 1
        i =0
        curr = curr.get_prev()
        list_str += "<-"
        while i < size - 1:
            list_str += str(curr)
            list_str += "<-"
            curr = curr.get_prev()
            i += 1
        if curr is self.head:
            list_str += "{True}"
        else: list_str += "{False}"
        return list_str

    def Bubble_sort(self):
        datalist = [0] * 6
        if self.head.get_data() is not None:    #Check if the list is empty
            datalist[1] += 1    #loop control comparison
            swapped = True                      #sets swap to true for loop
            datalist[2] += 1    #data assignment
            while swapped is True:              #do while swap is true
                datalist[1] += 1    #loop control comparison
                swapped = False                 #set swaps to false
                datalist[2] += 1   #data assignment #(no swaps have been made)
                curr = self.head                #start at the first node
                while curr.get_next() is not self.head: #while not at the end\
                    datalist[1] += 1    #loop control comparison
                    datalist[0] += 1    #data comparison
                    if curr.get_data() > curr.get_next().get_data(): 
                         #if n is less than n+1
                        swapped = True              #swaps is true
                        datalist[3] += 1   #data assignment involving loop
                        self.swapper(curr, curr.get_next())
                        datalist[2] += 3   #data assignment
                    curr = curr.get_next()      #increment current
                    datalist[3] += 1   #data assignment involving loop
        for i in range(0,4):
            datalist[5] += datalist[i]
        else: return datalist
    
    def Selection_sort(self):
        datalist = [0] * 6
        curr = self.head            #start at first node
        datalist[3] += 1    #data assignment involving loop
        while curr.get_next() is not self.head:    #do while not at end
            datalist[1] += 1 #loop control comparisons
            smallest = curr         #set smallest number to the current
            datalist[2] += 1 #data assignment
            curr2 = curr.get_next()     #curr2 is curr+1
            datalist[3] += 1 #data assignment involving loop
            if curr2 is not self.head:  #if curr2 is not the end
                                            #meaning curr is the last node
                datalist[1] += 1 #loop control comparison
                cont2 = True            #set continue to true for loop
                datalist[3] += 1 #data assignment involving loop
                while cont2:            #do while curr2 is not the last node
                    datalist[0] += 1 #data comparison
                    if curr2.get_data() < smallest.get_data():  #if curr2<smallest
                        smallest = curr2            #then set the curr to as smallest
                        datalist[2] += 1 #data assignment
                    curr2 = curr2.get_next()    #itterate curr2
                    datalist[3] += 1 #assignment involving loop
                    if curr2 is self.head:      #if curr2 is the head 
                        datalist[1] += 1
                        cont2 = False           #we have checked the list dont cont
            self.swapper(curr, smallest)
            datalist[2] += 3 #data assignments
            curr = curr.get_next()      #itterate curr
            datalist[3] += 1 #assignment involving loop
        for i in range(0,4):
            datalist[5] += datalist[i]
        return datalist
                
    def Insertion_sort(self):
        datalist = [0] * 6
        curr = self.head            #start at head node
        datalist[2] += 1    #assignments with data
        while curr is not self.head.get_prev(): #while curr is not the last 
            datalist[1] += 1 #loop control comparison
            curr = curr.get_next()  #curr is next node (needs to be saved)
            prev = curr.get_prev()  #previous is curr-1
            datalist[2] += 2 #assignments with data
            while prev is not self.head.get_prev(): #do while prev is not the last
                datalist[1] += 1 #loop control comparisons
                nextn = prev.get_next()     #next node is prev+1 
                datalist[2] += 1 #assignments with data
                if prev.get_data() > nextn.get_data():  #if n>n+1
                    datalist[0] += 1 #data comparisons
                    self.swapper(prev,nextn)
                    datalist[2] += 3 #assignments with data
                prev = prev.get_prev()      #prev = prev-1
                datalist[3] += 1 #assignments involving loop
        for i in range(0,4):
            datalist[5] += datalist[i]
        else: return datalist
        return datalist
                
    def Quick_sort(self):
        datalist = [0] * 6
        lastnode = self.head.get_prev()
        datalist[2] += 1 #assignment involving data
        headnode = self.head
        datalist = self.quick_sort_helper(headnode, lastnode, datalist)  #start with pivot at last node
        for i in range(0,4):
            datalist[5] += datalist[i]
        else: return datalist
        return datalist
        
    def quick_sort_helper(self, starti, endi, datalist):
        if starti is not endi:
            datalist[1] += 1 #loop control variable
            split_point = self.partition(starti, endi, datalist)
            datalist[4] += 1 #Other: function call
            if starti is not split_point:
                datalist[1] += 1 #loop control conparison
                datalist = self.quick_sort_helper(starti, split_point.get_prev(), datalist)
                datalist[4] += 1 #Other: function call
            if split_point is not endi:
                datalist[1] += 1 #loop control comparison
                datalist = self.quick_sort_helper(split_point.get_next(), endi, datalist)
                datalist[4] += 1 #Other: function call
        return datalist
        
    def partition(self, starti, endi, datalist):
        pivot_value = starti.get_data()
        datalist[2] += 1 #assignments with data
        left_mark = starti.get_next()
        datalist[2] += 1 #assignments with data
        right_mark = endi
        datalist[2] += 1 #assignments with data
        while True:
            while (((left_mark is not right_mark.get_next()))
                    and (left_mark.get_data() <= pivot_value)):
                datalist[1] += 1 #loop control comparison
                datalist[0] += 1 #data comparisons
                left_mark = left_mark.get_next()
                datalist[3] += 1 #assignments involving loop control
            while (((right_mark is not left_mark.get_prev()))
                    and (right_mark.get_data() >= pivot_value)):
                datalist[1] += 1 #loop control comparison
                datalist[0] += 1 #data comparisons
                right_mark = right_mark.get_prev()
                datalist[3] += 1 #assignments involving loop control
            if (right_mark is left_mark.get_prev()):
                datalist[0] += 1 #data comparisons
                break
            else:
                self.swapper(left_mark,right_mark)
                datalist[2] += 3 #assignments with data
        self.swapper(starti,right_mark)
        datalist[2] += 3 #assignments with data
        return right_mark
    
    
    def swapper(self,point1,point2):
        temp = point1.get_data()
        point1.set_data(point2.get_data())
        point2.set_data(temp)
        
    def Merge_sort(self):
        datalist = [0] * 6
        self.merge_sort_helper(self, datalist)
        datalist[4] +=1 #other: function call
        for i in range(0,4):
            datalist[5] += datalist[i]
        return datalist
        
    def merge_sort_helper(self, clist, datalist):
        datalist[1] += 1 #loop control comparison
        if clist.head is clist.head.get_prev(): 
            return
        i = -1
        datalist[2] += 1 #assignment involving data
        curr = clist.head
        datalist[3] += 1 #assignment involving loop control
        while curr is not clist.head.get_prev():
            datalist[1] += 1 #loop control comparison
            i += 1
            datalist[2] += 1 #assignment involving data
            curr = curr.get_next()
            datalist[3] += 1 #assignment involving loop control
        mid = (i//2)
        datalist[2] += 1 #assignment involving data
        i = 0
        datalist[2] += 1 #assignment involving data
        curr = clist.head
        datalist[3] += 1 #assignment involving loop control
        while i is not mid:
            datalist[1] += 1 #loop control comparison
            curr = curr.get_next()
            datalist[3] += 1 #assignment involving loop control
            i += 1
            datalist[2] += 1 #assignment involving data
        datalist[4] +=1 #other: function call
        left = self.get_list(clist.head, curr)
        datalist[2] += 1 #assignment involving data
        datalist[4] +=1 #other: function call
        right = self.get_list(curr.get_next(), clist.head.get_prev())
        datalist[2] += 1 #assignment involving data
        datalist[4] +=1 #other: function call
        self.merge_sort_helper(left, datalist)
        datalist[2] += 1 #assignment involving data
        datalist[4] +=1 #other: function call
        self.merge_sort_helper(right, datalist)
        datalist[2] += 1 #assignment involving data
        datalist[4] +=1 #other: function call
        self.merge(clist, left, right, datalist)
        
    
    def merge(self,clist, lefth, righth, datalist):
        curra = clist.head
        datalist[2] += 1 #assignment involving data
        currl = lefth.head
        datalist[2] += 1 #assignment involving data
        currr = righth.head
        datalist[2] += 1 #assignment involving data
        datalist[1] += 1 #loop control comparison
        while True:
            datalist[1] += 1 #loop control comparison
            datalist[0] += 1 #data comparison
            if currl.get_data() < currr.get_data():
                curra.set_data(currl.get_data())
                datalist[2] += 1 #assignment involving data
                datalist[0] += 1 #data comparison
                if currl.get_next() is lefth.head:
                    currl = righth.head.get_prev()
                    datalist[2] += 1 #assignment involving data
                else:
                    currl = currl.get_next()
                    datalist[3] += 1 #assignment involving loop
            else:
                curra.set_data(currr.get_data())
                datalist[2] += 1 #assignment involving data
                datalist[0] += 1 #data comparison
                if currr.get_next() is righth.head:
                    currr = lefth.head.get_prev()
                    datalist[2] += 1 #assignment involving data
                else:
                    currr = currr.get_next()
                    datalist[3] += 1 #assignment involving loop
            curra = curra.get_next()
            datalist[2] += 1 #assignment involving data
            datalist[1] += 1 #loop control comparison
            if curra is clist.head:
                break
#==============================================================================
#         print("in merge:")
#         print("startclist: %s" %(clist))
#         print("lefthalf: %s" %(lefth))
#         print("righthalf: %s" %(righth))
#         currl = lefth.head
#         print(currl)
#         currr = righth.head
#         print(currr)
#         curra = clist.head
#         print(curra)
#         firstrun = True
#         rightmove = False
#         leftmove = False
#         if (currl is lefth.head.get_prev()
#             or currr is righth.head.get_prev()):
#             print("in 1")
#             if currl.get_data() < currr.get_data():
#                 self.swapper(currl, curra)
#                 curra = curra.get_next()
#                 self.swapper(currr, curra)
#             else: 
#                 self.swapper(currr, curra)
#                 curra = curra.get_next()
#                 self.swapper(currl, curra)
#             leftmove = True
#             rightmove = True
#             firstrun = False
#         notlooped = True
#         while (((currl is not lefth.head
#             or currr is not righth.head) or firstrun) and notlooped):
#             if(currl is lefth.head.get_prev() or currr is righth.head.get_prev()):
#                 notlooped = False
#             firstrun = False
#             if currl.get_data() < currr.get_data():
#                 self.swapper(currl, curra)
#                 currl = currl.get_next()
#                 leftmove = True
#             else: 
#                 self.swapper(currr, curra)
#                 currr = currr.get_next()
#                 rightmove = True
#             curra = curra.get_next()
#             
#         
#         while (currl is not lefth.head or
#                             (not leftmove)):
#             self.swapper(currl, curra)
#             curra = curra.get_next()
#             currl = currl.get_next()
#             leftmove = True
#         while (currr is not righth.head or
#                             (not rightmove)):
#             self.swapper(currr, curra)
#             curra = curra.get_next()
#             currr = currr.get_next()
#             rightmove = True
#         print("endclist: %s" %(clist))
#==============================================================================
        
    def get_list(self, starti, endi):
        '''
        creates a new list with given start nd end nodes
        '''
        newlist = DLCList()
        notend = True
        while notend:
            newlist.add(starti.get_data())
            if starti is endi:
                notend = False
            starti = starti.get_next()
        return newlist
            
    def sorteddes(self, x):
        for i in range (1, x):
            self.add(i)
    def sortedaes(self, x):
        for i in range (x,1,-1):
            self.add(i)
    def randomsort(self, x):
        for i in range (1, x):
            self.add(rand.randint(0,100))
        
def makeplot(sers, title):
    x_locs = np.arange(1, 5)
    x_labels = [100, 200, 300, 500]
    f, ax = plt.subplots()
    ax.set_title(title)
    ax.set_ylabel("Time")
    ax.set_xlabel("List size N")
    ax.set_xticks(x_locs)
    ax.set_xticklabels(x_labels)
    for ser in sers:
        plt.plot(x_locs, ser, label=ser.name)
    plt.legend(loc=0)
    plt.savefig("%s.png" %(title))

def getaessortdata(sort,n):
    temp = DLCList()
    temp.sortedaes(n)
    return temp.sort()

aes_500 = '''
import time
from DoublyLinkedCircularList import Node
from DoublyLinkedCircularList import DLCList
Templist = DLCList()
Templist.sortedaes(100)'''
aes_1000 = '''
import time
from DoublyLinkedCircularList import Node
from DoublyLinkedCircularList import DLCList
Templist = DLCList()
Templist.sortedaes(200)'''
aes_5000 = '''
import time
from DoublyLinkedCircularList import Node
from DoublyLinkedCircularList import DLCList
Templist = DLCList()
Templist.sortedaes(300)'''
aes_10000 = '''
import time
from DoublyLinkedCircularList import Node
from DoublyLinkedCircularList import DLCList
Templist = DLCList()
Templist.sortedaes(500)'''
des_500 = '''
import time
from DoublyLinkedCircularList import Node
from DoublyLinkedCircularList import DLCList
Templist = DLCList()
Templist.sorteddes(100)'''
des_1000 = '''
import time
from DoublyLinkedCircularList import Node
from DoublyLinkedCircularList import DLCList
Templist = DLCList()
Templist.sorteddes(200)'''
des_5000 = '''
import time
from DoublyLinkedCircularList import Node
from DoublyLinkedCircularList import DLCList
Templist = DLCList()
Templist.sorteddes(300)'''
des_10000 = '''
import time
from DoublyLinkedCircularList import Node
from DoublyLinkedCircularList import DLCList
Templist = DLCList()
Templist.sorteddes(500)'''
rand_500 = '''
import time
from DoublyLinkedCircularList import Node
from DoublyLinkedCircularList import DLCList
Templist = DLCList()
Templist.randomsort(100)'''
rand_1000 = '''
import time
from DoublyLinkedCircularList import Node
from DoublyLinkedCircularList import DLCList
Templist = DLCList()
Templist.randomsort(200)'''
rand_5000 = '''
import time
from DoublyLinkedCircularList import Node
from DoublyLinkedCircularList import DLCList
Templist = DLCList()
Templist.randomsort(300)'''
rand_10000 = '''
import time
from DoublyLinkedCircularList import Node
from DoublyLinkedCircularList import DLCList
Templist = DLCList()
Templist.randomsort(500)'''

bubblesortcode = '''
Templist.Bubble_sort()
'''
selectionsortcode = '''
Templist.Selection_sort()
'''
insertionsortcode = '''
Templist.Insertion_sort()
'''
quicksortcode = '''
Templist.Quick_sort()
'''
mergesortcode = '''
Templist.Merge_sort()
'''

def main():
#==============================================================================
#     test2 = DLCList()
#     test2.sortedaes(30)
#     print(test2)
#     test2.Merge_sort()
#     print(test2)
#     test2 = DLCList()
#     test2.randomsort(200)
#     print(test2)
#     test2list = test2.Merge_sort()
#     print(test2list)
#==============================================================================
    
    
    aesl_500 = DLCList()
    aesl_1000 = DLCList()
    aesl_5000 = DLCList()
    aesl_10000 = DLCList()
    desl_500 = DLCList()
    desl_1000 = DLCList()
    desl_5000 = DLCList()
    desl_10000 = DLCList()
    randl_500 = DLCList()
    randl_1000 = DLCList()
    randl_5000 = DLCList()
    randl_10000 = DLCList()
    aesl_500.sortedaes(100)
    aesl_1000.sortedaes(200)
    aesl_5000.sortedaes(300)
    aesl_10000.sortedaes(500)
    desl_500.sorteddes(100)
    desl_1000.sorteddes(200)
    desl_5000.sorteddes(300)
    desl_10000.sorteddes(500)
    randl_500.randomsort(100)
    randl_1000.randomsort(200)
    randl_5000.randomsort(300)
    randl_10000.randomsort(500)
    bubbleaestime = [(timeit.timeit(setup = aes_500, stmt = bubblesortcode, number = 1)),
                  (timeit.timeit(setup = aes_1000, stmt = bubblesortcode, number = 1)),
                  (timeit.timeit(setup = aes_5000, stmt = bubblesortcode, number = 1)),
                  (timeit.timeit(setup = aes_10000, stmt = bubblesortcode, number = 1))]
    bubbledestime = [(timeit.timeit(setup = des_500, stmt = bubblesortcode, number = 1)),
                  (timeit.timeit(setup = des_1000, stmt = bubblesortcode, number = 1)),
                  (timeit.timeit(setup = des_5000, stmt = bubblesortcode, number = 1)),
                  (timeit.timeit(setup = des_10000, stmt = bubblesortcode, number = 1))]
    bubblerandtime = [(timeit.timeit(setup = rand_500, stmt = bubblesortcode, number = 1)),
                  (timeit.timeit(setup = rand_1000, stmt = bubblesortcode, number = 1)),
                  (timeit.timeit(setup = rand_5000, stmt = bubblesortcode, number = 1)),
                  (timeit.timeit(setup = rand_10000, stmt = bubblesortcode, number = 1))]
    
    bubbleaesdata = [aesl_500.Bubble_sort()[5], aesl_1000.Bubble_sort()[5],
                  aesl_5000.Bubble_sort()[5],aesl_10000.Bubble_sort()[5]]
    bubbledesdata = [desl_500.Bubble_sort()[5],desl_1000.Bubble_sort()[5],
                  desl_5000.Bubble_sort()[5],desl_10000.Bubble_sort()[5]]
    bubbleranddata = [randl_500.Bubble_sort()[5],randl_1000.Bubble_sort()[5],
                  randl_5000.Bubble_sort()[5],randl_10000.Bubble_sort()[5]]
    
    aesl_500 = DLCList()
    aesl_1000 = DLCList()
    aesl_5000 = DLCList()
    aesl_10000 = DLCList()
    desl_500 = DLCList()
    desl_1000 = DLCList()
    desl_5000 = DLCList()
    desl_10000 = DLCList()
    randl_500 = DLCList()
    randl_1000 = DLCList()
    randl_5000 = DLCList()
    randl_10000 = DLCList()
    aesl_500.sortedaes(100)
    aesl_1000.sortedaes(200)
    aesl_5000.sortedaes(300)
    aesl_10000.sortedaes(500)
    desl_500.sorteddes(100)
    desl_1000.sorteddes(200)
    desl_5000.sorteddes(300)
    desl_10000.sorteddes(500)
    randl_500.randomsort(100)
    randl_1000.randomsort(200)
    randl_5000.randomsort(300)
    randl_10000.randomsort(500)
    selectionaestime = [(timeit.timeit(setup = aes_500, stmt = selectionsortcode, number = 1)),
                  (timeit.timeit(setup = aes_1000, stmt = selectionsortcode, number = 1)),
                  (timeit.timeit(setup = aes_5000, stmt = selectionsortcode, number = 1)),
                  (timeit.timeit(setup = aes_10000, stmt = selectionsortcode, number = 1))]
    selectiondestime = [(timeit.timeit(setup = des_500, stmt = selectionsortcode, number = 1)),
                  (timeit.timeit(setup = des_1000, stmt = selectionsortcode, number = 1)),
                  (timeit.timeit(setup = des_5000, stmt = selectionsortcode, number = 1)),
                  (timeit.timeit(setup = des_10000, stmt = selectionsortcode, number = 1))]
    selectionrandtime = [(timeit.timeit(setup = rand_500, stmt = selectionsortcode, number = 1)),
                  (timeit.timeit(setup = rand_1000, stmt = selectionsortcode, number = 1)),
                  (timeit.timeit(setup = rand_5000, stmt = selectionsortcode, number = 1)),
                  (timeit.timeit(setup = rand_10000, stmt = selectionsortcode, number = 1))]
    selectionaesdata = [aesl_500.Selection_sort()[5], aesl_1000.Selection_sort()[5],
                  aesl_5000.Selection_sort()[5],aesl_10000.Selection_sort()[5]]
    selectiondesdata = [desl_500.Selection_sort()[5],desl_1000.Selection_sort()[5],
                  desl_5000.Selection_sort()[5],desl_10000.Selection_sort()[5]]
    selectionranddata = [randl_500.Selection_sort()[5],randl_1000.Selection_sort()[5],
                  randl_5000.Selection_sort()[5],randl_10000.Selection_sort()[5]]
    aesl_500 = DLCList()
    aesl_1000 = DLCList()
    aesl_5000 = DLCList()
    aesl_10000 = DLCList()
    desl_500 = DLCList()
    desl_1000 = DLCList()
    desl_5000 = DLCList()
    desl_10000 = DLCList()
    randl_500 = DLCList()
    randl_1000 = DLCList()
    randl_5000 = DLCList()
    randl_10000 = DLCList()
    aesl_500.sortedaes(100)
    aesl_1000.sortedaes(200)
    aesl_5000.sortedaes(300)
    aesl_10000.sortedaes(500)
    desl_500.sorteddes(100)
    desl_1000.sorteddes(200)
    desl_5000.sorteddes(300)
    desl_10000.sorteddes(500)
    randl_500.randomsort(100)
    randl_1000.randomsort(200)
    randl_5000.randomsort(300)
    randl_10000.randomsort(500)
    insertionaestime = [(timeit.timeit(setup = aes_500, stmt = insertionsortcode, number = 1)),
                  (timeit.timeit(setup = aes_1000, stmt = insertionsortcode, number = 1)),
                  (timeit.timeit(setup = aes_5000, stmt = insertionsortcode, number = 1)),
                  (timeit.timeit(setup = aes_10000, stmt = insertionsortcode, number = 1))]
    insertiondestime = [(timeit.timeit(setup = des_500, stmt = insertionsortcode, number = 1)),
                  (timeit.timeit(setup = des_1000, stmt = insertionsortcode, number = 1)),
                  (timeit.timeit(setup = des_5000, stmt = insertionsortcode, number = 1)),
                  (timeit.timeit(setup = des_10000, stmt = insertionsortcode, number = 1))]
    insertionrandtime = [(timeit.timeit(setup = rand_500, stmt = insertionsortcode, number = 1)),
                  (timeit.timeit(setup = rand_1000, stmt = insertionsortcode, number = 1)),
                  (timeit.timeit(setup = rand_5000, stmt = insertionsortcode, number = 1)),
                  (timeit.timeit(setup = rand_10000, stmt = insertionsortcode, number = 1))]
    insertionaesdata = [aesl_500.Insertion_sort()[5], aesl_1000.Insertion_sort()[5],
                  aesl_5000.Insertion_sort()[5],aesl_10000.Insertion_sort()[5]]
    insertiondesdata = [desl_500.Insertion_sort()[5],desl_1000.Insertion_sort()[5],
                  desl_5000.Insertion_sort()[5],desl_10000.Insertion_sort()[5]]
    insertionranddata = [randl_500.Insertion_sort()[5],randl_1000.Insertion_sort()[5],
                  randl_5000.Insertion_sort()[5],randl_10000.Insertion_sort()[5]]


    aesl_500 = DLCList()
    aesl_1000 = DLCList()
    aesl_5000 = DLCList()
    aesl_10000 = DLCList()
    desl_500 = DLCList()
    desl_1000 = DLCList()
    desl_5000 = DLCList()
    desl_10000 = DLCList()
    randl_500 = DLCList()
    randl_1000 = DLCList()
    randl_5000 = DLCList()
    randl_10000 = DLCList()
    aesl_500.sortedaes(100)
    aesl_1000.sortedaes(200)
    aesl_5000.sortedaes(300)
    aesl_10000.sortedaes(500)
    desl_500.sorteddes(100)
    desl_1000.sorteddes(200)
    desl_5000.sorteddes(300)
    desl_10000.sorteddes(500)
    randl_500.randomsort(100)
    randl_1000.randomsort(200)
    randl_5000.randomsort(300)
    randl_10000.randomsort(500)
    quickaestime = [(timeit.timeit(setup = aes_500, stmt = quicksortcode, number = 1)),
                  (timeit.timeit(setup = aes_1000, stmt = quicksortcode, number = 1)),
                  (timeit.timeit(setup = aes_5000, stmt = quicksortcode, number = 1)),
                  (timeit.timeit(setup = aes_10000, stmt = quicksortcode, number = 1))]
    quickdestime = [(timeit.timeit(setup = des_500, stmt = quicksortcode, number = 1)),
                  (timeit.timeit(setup = des_1000, stmt = quicksortcode, number = 1)),
                  (timeit.timeit(setup = des_5000, stmt = quicksortcode, number = 1)),
                  (timeit.timeit(setup = des_10000, stmt = quicksortcode, number = 1))]
    quickrandtime = [(timeit.timeit(setup = rand_500, stmt = quicksortcode, number = 1)),
                  (timeit.timeit(setup = rand_1000, stmt = quicksortcode, number = 1)),
                  (timeit.timeit(setup = rand_5000, stmt = quicksortcode, number = 1)),
                  (timeit.timeit(setup = rand_10000, stmt = quicksortcode, number = 1))]
    quickaesdata = [aesl_500.Quick_sort()[5], aesl_1000.Quick_sort()[5],
                  aesl_5000.Quick_sort()[5],aesl_10000.Quick_sort()[5]]
    quickdesdata = [desl_500.Quick_sort()[5],desl_1000.Quick_sort()[5],
                  desl_5000.Quick_sort()[5],desl_10000.Quick_sort()[5]]
    quickranddata = [randl_500.Quick_sort()[5],randl_1000.Quick_sort()[5],
                  randl_5000.Quick_sort()[5],randl_10000.Quick_sort()[5]]
    
    aesl_500 = DLCList()
    aesl_1000 = DLCList()
    aesl_5000 = DLCList()
    aesl_10000 = DLCList()
    desl_500 = DLCList()
    desl_1000 = DLCList()
    desl_5000 = DLCList()
    desl_10000 = DLCList()
    randl_500 = DLCList()
    randl_1000 = DLCList()
    randl_5000 = DLCList()
    randl_10000 = DLCList()
    aesl_500.sortedaes(100)
    aesl_1000.sortedaes(200)
    aesl_5000.sortedaes(300)
    aesl_10000.sortedaes(500)
    desl_500.sorteddes(100)
    desl_1000.sorteddes(200)
    desl_5000.sorteddes(300)
    desl_10000.sorteddes(500)
    randl_500.randomsort(100)
    randl_1000.randomsort(200)
    randl_5000.randomsort(300)
    randl_10000.randomsort(500)
    mergeaestime = [(timeit.timeit(setup = aes_500, stmt = mergesortcode, number = 1)),
                  (timeit.timeit(setup = aes_1000, stmt = mergesortcode, number = 1)),
                  (timeit.timeit(setup = aes_5000, stmt = mergesortcode, number = 1)),
                  (timeit.timeit(setup = aes_10000, stmt = mergesortcode, number = 1))]
    mergedestime = [(timeit.timeit(setup = des_500, stmt = mergesortcode, number = 1)),
                  (timeit.timeit(setup = des_1000, stmt = mergesortcode, number = 1)),
                  (timeit.timeit(setup = des_5000, stmt = mergesortcode, number = 1)),
                  (timeit.timeit(setup = des_10000, stmt = mergesortcode, number = 1))]
    mergerandtime = [(timeit.timeit(setup = rand_500, stmt = mergesortcode, number = 1)),
                  (timeit.timeit(setup = rand_1000, stmt = mergesortcode, number = 1)),
                  (timeit.timeit(setup = rand_5000, stmt = mergesortcode, number = 1)),
                  (timeit.timeit(setup = rand_10000, stmt = mergesortcode, number = 1))]
    mergeaesdata = [aesl_500.Merge_sort()[5], aesl_1000.Merge_sort()[5],
                  aesl_5000.Merge_sort()[5],aesl_10000.Merge_sort()[5]]
    mergedesdata = [desl_500.Merge_sort()[5],desl_1000.Merge_sort()[5],
                  desl_5000.Merge_sort()[5],desl_10000.Merge_sort()[5]]
    mergeranddata = [randl_500.Merge_sort()[5],randl_1000.Merge_sort()[5],
                  randl_5000.Merge_sort()[5],randl_10000.Merge_sort()[5]]
    
    
    s1 = pd.Series(bubbleaestime, index = [100,200,300,500], name = "bubble")
    s2 = pd.Series(insertionaestime, index = [100,200,300,500], name = "insertion")
    s3 = pd.Series(selectionaestime, index = [100,200,300,500], name = "selection")
    s4 = pd.Series(quickaestime, index = [100,200,300,500], name = "quick")
    s5 = pd.Series(mergeaestime, index = [100,200,300,500], name = "merge")
    ascendingpd = [s1,s2,s3,s4,s5]
    makeplot(ascendingpd, "Sort Time vs. Ascending List")
    
    s1 = pd.Series(bubbledestime, index = [100,200,300,500], name = "bubble")
    s2 = pd.Series(insertiondestime, index = [100,200,300,500], name = "insertion")
    s3 = pd.Series(selectiondestime, index = [100,200,300,500], name = "selection")
    s4 = pd.Series(quickdestime, index = [100,200,300,500], name = "quick")
    s5 = pd.Series(mergedestime, index = [100,200,300,500], name = "merge")
    descendingpd = [s1,s2,s3,s4,s5]
    makeplot(descendingpd, "Sort Time vs. Descending List")
    
    s1 = pd.Series(bubblerandtime, index = [100,200,300,500], name = "bubble")
    s2 = pd.Series(insertionrandtime, index = [100,200,300,500], name = "insertion")
    s3 = pd.Series(selectionrandtime, index = [100,200,300,500], name = "selection")
    s4 = pd.Series(quickrandtime, index = [100,200,300,500], name = "quick")
    s5 = pd.Series(mergerandtime, index = [100,200,300,500], name = "merge")
    randompd = [s1,s2,s3,s4,s5]
    makeplot(randompd, "Sort Time vs. Random List")
    
    s1 = pd.Series(bubbleaesdata, index = [100,200,300,500], name = "bubble")
    s2 = pd.Series(insertionaesdata, index = [100,200,300,500], name = "insertion")
    s3 = pd.Series(selectionaesdata, index = [100,200,300,500], name = "selection")
    s4 = pd.Series(quickaesdata, index = [100,200,300,500], name = "quick")
    s5 = pd.Series(mergeaesdata, index = [100,200,300,500], name = "merge")
    ascendingpd = [s1,s2,s3,s4,s5]
    makeplot(ascendingpd, "Sort Operations vs. Ascending List")
    
    s1 = pd.Series(bubbledesdata, index = [100,200,300,500], name = "bubble")
    s2 = pd.Series(insertiondesdata, index = [100,200,300,500], name = "insertion")
    s3 = pd.Series(selectiondesdata, index = [100,200,300,500], name = "selection")
    s4 = pd.Series(quickdesdata, index = [100,200,300,500], name = "quick")
    s5 = pd.Series(mergedesdata, index = [100,200,300,500], name = "merge")
    descendingpdo = [s1,s2,s3,s4,s5]
    makeplot(descendingpdo, "Sort Operations vs. Descending List")
    
    s1 = pd.Series(bubbleranddata, index = [100,200,300,500], name = "bubble")
    s2 = pd.Series(insertionranddata, index = [100,200,300,500], name = "insertion")
    s3 = pd.Series(selectionranddata, index = [100,200,300,500], name = "selection")
    s4 = pd.Series(quickranddata, index = [100,200,300,500], name = "quick")
    s5 = pd.Series(mergeranddata, index = [100,200,300,500], name = "merge")
    randompdo = [s1,s2,s3,s4,s5]
    makeplot(randompdo, "Sort Operations vs. Random List")



    aesl_500 = DLCList()
    aesl_1000 = DLCList()
    aesl_5000 = DLCList()
    aesl_10000 = DLCList()
    desl_500 = DLCList()
    desl_1000 = DLCList()
    desl_5000 = DLCList()
    desl_10000 = DLCList()
    randl_500 = DLCList()
    randl_1000 = DLCList()
    randl_5000 = DLCList()
    randl_10000 = DLCList()
    aesl_500.sortedaes(100)
    aesl_1000.sortedaes(200)
    aesl_5000.sortedaes(300)
    aesl_10000.sortedaes(500)
    desl_500.sorteddes(100)
    desl_1000.sorteddes(200)
    desl_5000.sorteddes(300)
    desl_10000.sorteddes(500)
    randl_500.randomsort(100)
    randl_1000.randomsort(200)
    randl_5000.randomsort(300)
    randl_10000.randomsort(500)
    bubblefulldata = [aesl_500.Bubble_sort(), aesl_1000.Bubble_sort(),
                  aesl_5000.Bubble_sort(),aesl_10000.Bubble_sort(),
                  desl_500.Bubble_sort(),desl_1000.Bubble_sort(),
                  desl_5000.Bubble_sort(),desl_10000.Bubble_sort(),
                  randl_500.Bubble_sort(),randl_1000.Bubble_sort(),
                  randl_5000.Bubble_sort(),randl_10000.Bubble_sort()]
    index_list = ['Ascendeding N = 100', 'Ascendeding N = 200', 'Ascendeding N = 300','Ascendeding N = 500','Descending N = 100'
                  , 'Descending N = 200', 'Descending N = 300','Descending N = 500', 'Random N = 100', 'Random N = 200', 'Random N = 300','Random N = 500']
    col_list = ['#Data Comparisons','#Loop Comparisons','#Data assignments','#Loop Control','#Other','Total Operations']
    bubbledatafram = pd.DataFrame(bubblefulldata, columns = col_list, index = index_list)

    aesl_500 = DLCList()
    aesl_1000 = DLCList()
    aesl_5000 = DLCList()
    aesl_10000 = DLCList()
    desl_500 = DLCList()
    desl_1000 = DLCList()
    desl_5000 = DLCList()
    desl_10000 = DLCList()
    randl_500 = DLCList()
    randl_1000 = DLCList()
    randl_5000 = DLCList()
    randl_10000 = DLCList()
    aesl_500.sortedaes(100)
    aesl_1000.sortedaes(200)
    aesl_5000.sortedaes(300)
    aesl_10000.sortedaes(500)
    desl_500.sorteddes(100)
    desl_1000.sorteddes(200)
    desl_5000.sorteddes(300)
    desl_10000.sorteddes(500)
    randl_500.randomsort(100)
    randl_1000.randomsort(200)
    randl_5000.randomsort(300)
    randl_10000.randomsort(500)
    selectionfulldata = [aesl_500.Selection_sort(), aesl_1000.Selection_sort(),
                  aesl_5000.Selection_sort(),aesl_10000.Selection_sort(),
                  desl_500.Selection_sort(),desl_1000.Selection_sort(),
                  desl_5000.Selection_sort(),desl_10000.Selection_sort(),
                  randl_500.Selection_sort(),randl_1000.Selection_sort(),
                  randl_5000.Selection_sort(),randl_10000.Selection_sort()]
    selectiondatafram = pd.DataFrame(selectionfulldata, columns = col_list, index = index_list)

    aesl_500 = DLCList()
    aesl_1000 = DLCList()
    aesl_5000 = DLCList()
    aesl_10000 = DLCList()
    desl_500 = DLCList()
    desl_1000 = DLCList()
    desl_5000 = DLCList()
    desl_10000 = DLCList()
    randl_500 = DLCList()
    randl_1000 = DLCList()
    randl_5000 = DLCList()
    randl_10000 = DLCList()
    aesl_500.sortedaes(100)
    aesl_1000.sortedaes(200)
    aesl_5000.sortedaes(300)
    aesl_10000.sortedaes(500)
    desl_500.sorteddes(100)
    desl_1000.sorteddes(200)
    desl_5000.sorteddes(300)
    desl_10000.sorteddes(500)
    randl_500.randomsort(100)
    randl_1000.randomsort(200)
    randl_5000.randomsort(300)
    randl_10000.randomsort(500)
    insertionfulldata = [aesl_500.Insertion_sort(), aesl_1000.Insertion_sort(),
                  aesl_5000.Insertion_sort(),aesl_10000.Insertion_sort(),
                  desl_500.Insertion_sort(),desl_1000.Insertion_sort(),
                  desl_5000.Insertion_sort(),desl_10000.Insertion_sort(),
                  randl_500.Insertion_sort(),randl_1000.Insertion_sort(),
                  randl_5000.Insertion_sort(),randl_10000.Insertion_sort()]
    insertiondatafram = pd.DataFrame(insertionfulldata, columns = col_list, index = index_list)
    
    aesl_500 = DLCList()
    aesl_1000 = DLCList()
    aesl_5000 = DLCList()
    aesl_10000 = DLCList()
    desl_500 = DLCList()
    desl_1000 = DLCList()
    desl_5000 = DLCList()
    desl_10000 = DLCList()
    randl_500 = DLCList()
    randl_1000 = DLCList()
    randl_5000 = DLCList()
    randl_10000 = DLCList()
    aesl_500.sortedaes(100)
    aesl_1000.sortedaes(200)
    aesl_5000.sortedaes(300)
    aesl_10000.sortedaes(500)
    desl_500.sorteddes(100)
    desl_1000.sorteddes(200)
    desl_5000.sorteddes(300)
    desl_10000.sorteddes(500)
    randl_500.randomsort(100)
    randl_1000.randomsort(200)
    randl_5000.randomsort(300)
    randl_10000.randomsort(500)
    quickfulldata = [aesl_500.Quick_sort(), aesl_1000.Quick_sort(),
                  aesl_5000.Quick_sort(),aesl_10000.Quick_sort(),
                  desl_500.Quick_sort(),desl_1000.Quick_sort(),
                  desl_5000.Quick_sort(),desl_10000.Quick_sort(),
                  randl_500.Quick_sort(),randl_1000.Quick_sort(),
                  randl_5000.Quick_sort(),randl_10000.Quick_sort()]
    quickdatafram = pd.DataFrame(quickfulldata, columns = col_list, index = index_list)
    
    aesl_500 = DLCList()
    aesl_1000 = DLCList()
    aesl_5000 = DLCList()
    aesl_10000 = DLCList()
    desl_500 = DLCList()
    desl_1000 = DLCList()
    desl_5000 = DLCList()
    desl_10000 = DLCList()
    randl_500 = DLCList()
    randl_1000 = DLCList()
    randl_5000 = DLCList()
    randl_10000 = DLCList()
    aesl_500.sortedaes(100)
    aesl_1000.sortedaes(200)
    aesl_5000.sortedaes(300)
    aesl_10000.sortedaes(500)
    desl_500.sorteddes(100)
    desl_1000.sorteddes(200)
    desl_5000.sorteddes(300)
    desl_10000.sorteddes(500)
    randl_500.randomsort(100)
    randl_1000.randomsort(200)
    randl_5000.randomsort(300)
    randl_10000.randomsort(500)
    mergefulldata = [aesl_500.Merge_sort(), aesl_1000.Merge_sort(),
                  aesl_5000.Merge_sort(),aesl_10000.Merge_sort(),
                  desl_500.Merge_sort(),desl_1000.Merge_sort(),
                  desl_5000.Merge_sort(),desl_10000.Merge_sort(),
                  randl_500.Merge_sort(),randl_1000.Merge_sort(),
                  randl_5000.Merge_sort(),randl_10000.Merge_sort()]
    mergedatafram = pd.DataFrame(mergefulldata, columns = col_list, index = index_list)
    
    index_listtb = ['Bub','Aes ', 'Des', 'Rand']
    index_listts = ['Sel','Aes', 'Des', 'Rand']
    index_listti = ['Ins','Aes', 'Des', 'Rand']
    index_listtq = ['Qui','Aes', 'Des', 'Rand']
    index_listtm = ['Mer','Aes', 'Des', 'Rand']
    col_listt =['100', '200','300', '500']
    spacelist = ['-','-','-','-']
    pd0 = pd.DataFrame([spacelist,bubbleaestime, bubbledestime, bubblerandtime], columns = col_listt, index = index_listtb)
    pd1 = pd.DataFrame([spacelist,insertionaestime, insertiondestime, insertionrandtime], columns = col_listt, index = index_listts)
    pd2 = pd.DataFrame([spacelist,selectionaestime, selectiondestime, insertionrandtime], columns = col_listt, index = index_listti)
    pd3= pd.DataFrame([spacelist,quickaestime, quickdestime, insertionrandtime], columns = col_listt, index = index_listtq)
    pd4 = pd.DataFrame([spacelist,mergeaestime, mergedestime, insertionrandtime], columns = col_listt, index = index_listtm)
    pds = [pd0,pd1,pd2,pd3,pd4]
    timespd = pd.concat(pds)
    
    
    timespd.to_csv("SortTimes.csv")
    bubbledatafram.to_csv("BubbleData.csv")
    insertiondatafram.to_csv("InsertionData.csv")
    selectiondatafram.to_csv("SelectionData.csv")
    quickdatafram.to_csv("QuickData.csv")
    mergedatafram.to_csv("MergeData.csv")
    
#==============================================================================
#     aes_500qukdata
#     aes_1000qukdata
#     aes_5000qukdata
#     aes_10000qukdata
#     des_500qukdata
#     des_1000qukdata
#     des_5000qukdata
#     des_10000qukdata
#     rand_500qukdata
#     rand_1000qukdata
#     rand_5000qukdata
#     rand_10000qukdata
#==============================================================================
#==============================================================================
#     bubbletime = (timeit.timeit(setup = mysetup,
#                         stmt = bubblesortcode,
#                         number = 1))
#     selectiontime = (timeit.timeit(setup = mysetup,
#                         stmt = selectionsortcode,
#                         number = 1))
#     insertiontime = (timeit.timeit(setup = mysetup,
#                         stmt = insertionsortcode,
#                         number = 1))
#     quicktime = (timeit.timeit(setup = mysetup,
#                         stmt = quicksortcode,
#                         number = 1))
#     print("Bubble: ") 
#     print(bubbletime)
#     print("Selection: ") 
#     print(selectiontime)
#     print("Insertion: ")
#     print(insertiontime)
#     print("Quick: ") 
#     print(quicktime)
#     
#==============================================================================
    
#==============================================================================
#     groceries = DLCList()
#     groceries.add("eggs")
#     groceries.add("milk")
#     groceries.add("bacon")
#     groceries.add("cookies")
#     print(groceries)
#     groceries.append("burger")
#     print(groceries)
#     groceries.remove("bacon")
#     print(groceries)
#     print(groceries.size())
#     print(groceries.pop(3))
#     print(groceries)
#     groceries.insert(3,"hotdog")
#     print(groceries)
#     print(groceries.is_empty())
#     print(groceries.search("cookies"))
#     print(groceries.get_indicies())
#     print(groceries.see_head())
#     print(groceries.search("guns"))
#     print(groceries.is_circular())
#==============================================================================

main()
