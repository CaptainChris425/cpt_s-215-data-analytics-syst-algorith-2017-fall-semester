# -*- coding: utf-8 -*-

from Mapclass import *
def main():
    keyboard = WordPredictor()

    keyboard.train("test.txt")
    keyboard.build()
    print("Start Typing(0 to quit): ")
    x = 'g'
    while x is not '0':
        x = input("~")
        print(keyboard.get_best(str(x)))

main()