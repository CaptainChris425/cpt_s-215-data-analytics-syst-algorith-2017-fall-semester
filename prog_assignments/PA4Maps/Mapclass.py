# -*- coding: utf-8 -*-

class DictEntry:
    def __init__(self, word, prob):
        self.word = word
        self.prob = prob
    def get_word(self):
        return self.word
    def get_prob(self):
        return self.prob
    def match_pattern(self, pattern):
        patternlist = list(pattern)
        wordlist = list(self.word)
        if len(patternlist) > len(wordlist):
            return False
        for i in range(len(patternlist)):
            if patternlist[i] != wordlist[i]:
                return False
        return True
    def __str__(self):
        return ("%s" %(self.word))
    def __int__(self):
        return self.word.__hash__()
    
class HashTable:
    def __init__(self, size = 11):
        self.slots = [[None]]
        for i in range(size-1):
            self.slots.append([None])
        self.size = size
    def __str__(self):
        return str(self.slots)
    def put(self, item):
        hashvalue = self.hashfunction(item)
        for i in range(0,len(self.slots[hashvalue])-1):
            if self.slots[hashvalue][i] == item:
                return hashvalue
        self.slots[hashvalue].insert(0,item)
        return hashvalue
    def get(self, item):
        hashvalue = self.hashfunction(item)
        for i in range(0, len(self.slots[hashvalue])-1):
            if self.slots[hashvalue][i] == item:
                return hashvalue
        return -1
    def remove(self, item):
        hashvalue = self.hashfunction(item)
        for i in range(0,len(self.slots[hashvalue])-1):
            if self.slots[hashvalue][i] == item:
                del self.slots[hashvalue][i]
                #self.slots[hashvalue][i] = None
                return hashvalue
        return -1
    def hashfunction(self, item):
        item = str(item)
        if type(item) is str:
            return item.__hash__() % self.size
        else:
            return int(int(item) % self.size)
        
    def getind(self, x = 0, y = 0):
        return self.slots[x][y]
    def getslots(self):
        return self.slots
    def hasval(self, val):
        location = self.hashfunction(val)
        for i in range(len(self.slots[location])):
            if self.slots[location][i] == val:
                return True
        return False    
            
class Map(HashTable):
    def __init__(self, size = 11):
        super().__init__(size)
        self.values = [[None]]
        for i in range(size-1):
            self.values.append([None])
    def __str__(self):
        s = ""
        for i in range(len(self.values)):
            for j in range(len(self.values[i])-1):
                s += (str(super().getind(i,j))+ ":" + str(self.values[i][j]) + ",")
        return s
    def __getitem__(self, key):
        return self.get(key)
    def __setitem__(self, key, data):
        self.put(key,data)
    def __len__(self):
        return len(self.values)
    def __delitem__(self, key):
        self.remove(key)
    def __contains__(self, key):
        return self.contains(key)
    def get(self, key):
        if not super().hasval(key):
            return None
        location = super().get(key)
        for i in range(0, len(super().getslots()[location])):
            if super().getslots()[location][i] == key:
                value = (self.values[location][i])
        return value
    def put(self, key, data):
        if super().hasval(key):
            location = super().put(key)
            for i in range(len(super().getslots()[location])):
                if super().getslots()[location][i] == key:
                    self.values[location][i] = data
                    return -1   
        location = super().put(key)
        if location != -1:
            self.values[location].insert(0,data)
        return -1
    def remove(self, key):
        if super().hasval(key):
            location = super().put(key)
            for i in range(len(super().getslots()[location])):
                if super().getslots()[location][i] == key:
                    del self.values[location][i]
        super().remove(key)
    def contains(self, key):
        return super().hasval(key)


class WordPredictor():
    def __init__(self, size = 11):
        self.word_to_count = Map(size)
        self.prefix_to_entry1 = Map(size)
        self.prefix_to_entry2 = Map(size)
        self.prefix_to_entry3 = Map(size)
        self.total = 0
        self.words = ''
    def train(self,training_file):   #train on file
        try: 
            infile = open(str(training_file),'r')
            self.words = infile.read()
            self.words = self.words.split()
            for word in self.words:
                self.train_word(word)
        except IOError: print("could not open file")
        
            
        
    def train_word(self, word):       #train on single word
        if word not in self.words:
            self.words.append(word)
        self.total += 1
        word = word.strip(",.!?:;'")
        word = word.lower()
        if word in self.word_to_count:
            self.word_to_count[word] += 1
        else:
            self.word_to_count[word] = 1
            
    def get_training_count(self):    #total words
        return self.total
    def get_word_count(self, word):
        if word in self.word_to_count:
            return self.word_to_count[word]
        else: return 0
    def build(self):
        self.words = set(self.words)
        self.words = list(self.words)
        for word in self.words:
            word = word.strip(",.!?:;'")
            word = word.lower()
            entry = DictEntry(word , self.word_to_count[str(word)]/ self.total)
            prefix = []
            s = ''
            for i in word:
                s+=i
                prefix.append(s)
            for fix in prefix:
                put = False
                if fix in self.prefix_to_entry1:
                    if self.prefix_to_entry1[fix].get_prob() < entry.get_prob():
                        self.prefix_to_entry1[fix] = entry
                else:
                    self.prefix_to_entry1[fix] = entry
                    
                    
                if fix in self.prefix_to_entry2:
                    if (self.prefix_to_entry2[fix].get_prob() < entry.get_prob()
                        and entry.get_prob() < self.prefix_to_entry1[fix].get_prob()):
                        self.prefix_to_entry2[fix] = entry
                else:
                    
                    self.prefix_to_entry2[fix] = entry
                    
                    
                if fix in self.prefix_to_entry3:
                    if (self.prefix_to_entry3[fix].get_prob() < entry.get_prob()
                        and entry.get_prob() < self.prefix_to_entry2[fix].get_prob()):
                        self.prefix_to_entry3[fix] = entry
                else:
                    if entry.get_prob() < self.prefix_to_entry2[fix].get_prob():
                        self.prefix_to_entry3[fix] = entry
                
#==============================================================================
#                 if fix in self.prefix_to_entry1 and not put:
#                     if self.prefix_to_entry1[fix].get_prob() < entry.get_prob():
#                         self.prefix_to_entry1[fix] = entry
#                         put = True
#                 elif fix not in self.prefix_to_entry1 or not put:
#                     self.prefix_to_entry1[fix] = entry
#                 if fix in self.prefix_to_entry2 and not put:
#                     if self.prefix_to_entry2[fix].get_prob() < entry.get_prob():
#                         self.prefix_to_entry2[fix] = entry
#                         put = True
#                 elif fix not in self.prefix_to_entry2 or not put:
#                     self.prefix_to_entry2[fix] = entry
#                 if fix in self.prefix_to_entry3 and not put:
#                     if self.prefix_to_entry3[fix].get_prob() < entry.get_prob():
#                         self.prefix_to_entry3[fix] = entry
#                         put = True
#                 elif fix not in self.prefix_to_entry3 or not put:
#                     self.prefix_to_entry3[fix] = entry
#==============================================================================
                    
                
    def get_best(self, prefix):       #return most likely given prefix
        if prefix in self.prefix_to_entry1:
            print(self.prefix_to_entry1[prefix])
            print(self.prefix_to_entry2[prefix])
            print(self.prefix_to_entry3[prefix])
            x = int(input("Please choose a word 1,2, or 3: "))
            if x == 1:
                return self.prefix_to_entry1[prefix]
            elif x == 2:
                return self.prefix_to_entry2[prefix]
            elif x == 3:
                return self.prefix_to_entry3[prefix]
        else:
            return DictEntry("Word not found" , 0 )



#==============================================================================
# test = Map(100)
# test[5] = 10
# test[4] = 7
# print(test)
# del test[5]
# del test[6]
# print(test)
# print(7 in test)
# print(5 in test)
# 
#==============================================================================
#==============================================================================
# test = WordPredictor()
# test.train("moby_end.txt")
# test.build()
# print(test.get_best("coi"))
#==============================================================================
