# -*- coding: utf-8 -*-

import pandas as pd
from collections import Counter
import numpy as np
import matplotlib.pyplot as plt
import math
from random import *
import subprocess
from sklearn.tree import DecisionTreeClassifier, export_graphviz
from sklearn.model_selection import cross_val_score



def open_file_DF(filename = None):
    '''
    opens a file(csv format) and returns a
    dataframe
    '''
    if filename is None:
        input_file = input("File to Open: ")
        try:
            with open("%s" %(input_file)) as a_file:
                dataframe = pd.read_csv(a_file)
                return dataframe
        except IOError:
            print("file could not be open")
    else:
        try:
            with open("%s" %(filename)) as a_file:
                dataframe = pd.read_csv(a_file)
                return dataframe
        except IOError:
            print("file could not be open")

def calc_prob(class_col):
    '''
    calculates the probability given an
    entire dataframe column of a value occuring
    '''
    examples = len(class_col)
    count = list(Counter(class_col).values())
    return np.array(count)/examples
    
def calc_entropy(instances, class_label = 0):
    '''
    with instances as dataframe and a column name
    returns the entropy within given column
    '''
    probabilities = calc_prob(instances[class_label])
    summation = [(-i * math.log(i,2)) for i in probabilities if i]
    entropy = np.sum(summation)
    return entropy
def dict_entropy(instances, class_list):
    '''
    with instances as dataframe
    returns a dictionary with column names as keys
    and entropies as values
    '''
    entropy_list = []
    for label in class_list:
        entropy_list.append(calc_entropy(instances, label))
    entropy_dict = dict(zip(class_list, entropy_list))
    return entropy_dict
    
def min_entropy_partition(instances, class_list = None):   
    '''
    with instances as dataframe and classlist as column name
    returns the column name with minimum entropy
    '''
    entropies = dict_entropy(instances, class_list)
    return min(entropies, key = entropies.get)

def partition_by(inputs, attribute):
    '''
    splits a DF in a dataframe split on an
    attribute of the origional DF
    '''
    i = 0
    subsets = {}
    for data in inputs[attribute]:
        if data in subsets:
            subsets[data] = inputs[inputs[attribute] == data]
        else:
            subsets[data] = pd.DataFrame(inputs.iloc[i])

        i += 1
    return (subsets)

class node():
    '''
    Decision Tree node class
    label = attribute label
    childs = list of nodes with index equaling
    the attribute value
    '''
    def __init__(self, label = None, child = None, index = 0):
        self.label = label
        self.childs = []
        if child is not None:
            while index > len(self.childs):
                self.childs.append(node(label = None))
            self.childs[index] = child
    def set_child(self, child = None, index = 0):
        if child is not None:
            while index >= len(self.childs):
                self.childs.append(None)
            self.childs[index] = child
    def set_label(self, label = None):
        self.label = label
    def get_child(self, index = 0):
        while index > len(self.childs):
            index = index % len(self.childs)
        return self.childs[index]
    def get_label(self):
        return self.label
    def get_numchld(self):
        return len(self.childs)
    
    
class DecisionTree():
    def __init__(self, root = None):
        self.root = node()
        
    def build(self, curr = None, inputs = None,
              split_candidates=None, condition = None):
        '''
        recursively builds a decision tree where nodes are created in 
        order of lowest entropy and splitting the data on a specified 
        attribute value
        '''
        if curr is None:
            curr = self.root
            
        if inputs is None:
            inputs = open_file_DF()
            
        if split_candidates is None:
            # this is the first pass
            split_candidates = list(inputs)
            
        if condition is None:
            condition = split_candidates.pop()
            
        num_examples = len(inputs)
        # count Trues and Falses in the examples
        num_trues = 0
        if len(inputs.columns) > 1:
            for i in inputs[condition]:
                if i == True: 
                    num_trues += 1
        num_falses = num_examples - num_trues
        
        if num_trues == 0: # no trues, this is a False leaf node
            return node(label = 'False')
        if num_falses == 0: # no falses, this is a True leaf node
            return node(label = 'True')
        
        # part (2) in the ID3 algorithm -> list of attributes is empty -> leaf node with majority class label
        if not split_candidates: 
            return node(label = str(num_trues >= num_falses))
        
        # part (3) in ID3 algorithm -> split on best attribute
        split_attribute = min_entropy_partition(inputs, split_candidates)
        partitions = partition_by(inputs, split_attribute)
        split_candidates.remove(split_attribute)
        if curr.get_label() is None:
            curr.set_label(split_attribute)
        
        # recursively build the subtrees
        for attribute_value in partitions.keys():
            curr.set_child(index = attribute_value, 
                           child = self.build( curr = node(),inputs = partitions[attribute_value],
                                              split_candidates=split_candidates,
                                              condition = condition))
        return (curr)
        
    
    def printer(self , curr = None, val = None):
        '''
        recursively prints the decision tree 
        '''
        #print("in printer")
        if curr == None:
            print("root attribute : " , self.root.get_label())
            curr = self.root
        if curr.get_label() == None:
            return
        if val is not None:
            print('attribute for val: ',val,' : ' , curr.get_label())
        for i in range(curr.get_numchld()):
            if curr.get_child(i) is not None:
                print('|' , i , '|' , end = '') #each attribute value separated by ||
        print('\n')
        for i in range(curr.get_numchld()):
            if curr.get_child(i) is not None:
                self.printer(curr = curr.get_child(i), val = i)
        return
    
    def classify(self, new_data = None, curr = None, number = 0):
        '''
        with new_data as a dataframe, iteritevly classifies 
        a row within DF specified by number
        returns the predicted outcome of row
        '''
        if curr == None:
            curr = self.root
        conditions = list(new_data.columns)
        while (curr.get_label() is not 'True') and (curr.get_label() is not 'False'):
            if curr.get_label() in conditions:
                leaf = list(new_data[curr.get_label()])[number]
                curr = curr.get_child(index = leaf)
                if curr is None:
                    break
        if curr is not None:
            #return str(question) + '? : ' + curr.get_label()
            return 1 if curr.get_label() == 'True' else 0
        else:
            #return str(question) + '? : ' + str(bool(randint(1,100)%2))
            return int(bool(randint(1,100)%2))
            
                       
                       
                       
                       
                       
                       
                    
def evaluationmetrics(traindt, df):
    '''
    takes in a decision tree and a test dataframe
    returns a dictionary of keys and values
    {Accuracy, Precision, Recall, F1}
    '''
    metrics = ['Accuracy', 'Precision', 'Recall', 'F1']
    values = [0,0,0,0]
    testdf = df.copy()
    x = list(testdf.columns)
    condition = x.pop()
    correct = 0
    incorrect = 0
    truepositives = 0
    falsepositives = 0
    falsenegatives = 0
    for i in range(0, len(df)):
        trueval = testdf[condition][i]
        testval = traindt.classify(testdf, number = i)
        #print(i, 'testval : %s ; trueval : %s' %(testval,trueval), end = ' = ')
        if trueval == testval: 
            #print("correct")
            correct += 1
        if trueval != testval: 
            incorrect += 1
            #print("incorrect")
        if testval == 1 and trueval == 1: 
            truepositives += 1
            #print("T.P")
        if testval == 1 and trueval == 0: 
            #print("F.P")
            falsepositives += 1
        if testval == 0 and trueval == 1: 
            #print("F.N")
            falsenegatives += 1
        testdf.drop([0])
        
    #print(correct, incorrect, truepositives, falsepositives, falsenegatives)
    values[0] = correct/(correct + incorrect)
    values[1] = truepositives/(truepositives+falsepositives)
    values[2] = truepositives/(truepositives+falsenegatives)
    values[3] = (2*values[1]*values[2])/(values[1]+values[2])
    return dict(zip(metrics, values))
        
    
    
    
          
def encode_target(df, target_column):
    """Add column to df with integers for the target.

    Args
    ----
    df -- pandas DataFrame.
    target_column -- column to map to int, producing
                     new Target column.

    Returns
    -------
    df_mod -- modified DataFrame.
    targets -- list of target names.
    """
    df_mod = df.copy()
    targets = df_mod[target_column].unique()
    map_to_int = {name: n for n, name in enumerate(targets)}
    df_mod["Target"] = df_mod[target_column].replace(map_to_int)

    return (df_mod, targets)     

def visualize_tree(tree, feature_names):
    """Create tree png using graphviz.

    Args
    ----
    tree -- scikit-learn DecsisionTree.
    feature_names -- list of feature names.
    """
    with open("dt.dot", 'w') as f:
        export_graphviz(tree, out_file=f,
                        feature_names=feature_names)

    command = ["dot", "-Tpng", "dt.dot", "-o", "dt.png"]
    try:
        subprocess.check_call(command)
    except:
        exit("Could not run dot, ie graphviz, to "
             "produce visualization")

def getavgmetrics(tree = None,x = None,y= None, kval = 0):
    '''
    takes in a Sklearn decision tree and the data columns as x
    and the condition column as y with Kval as how many values of k to test
    returns a dictionary of keys and list of values
    {Accuracy, Precision, Recall, F1}
    '''
    accuracylist = []
    precisionlist = []
    recalllist = []
    f1list = []
    avgmetrics = {}
    for i in range(2,kval+1):
        print(" ", end = '')
        accuracies = cross_val_score(tree, x, y, cv=i)
        precision = cross_val_score(tree, x, y, cv=i, scoring = 'precision')
        recall = cross_val_score(tree, x, y, cv=i, scoring = 'recall')
        f1 = cross_val_score(tree, x, y, cv=i, scoring = 'f1')
        accuracylist.append(sum(accuracies)/len(accuracies))
        precisionlist.append(sum(precision)/len(precision))
        recalllist.append(sum(recall)/len(recall))
        f1list.append(sum(f1)/len(f1))
    avgmetrics['accuracies'] = accuracylist
    avgmetrics['precision'] = precisionlist
    avgmetrics['recall'] = recalllist
    avgmetrics['f1'] = f1list
    return avgmetrics

def creategraphs(metrics):
    '''
    takes in a dictionary of metrics and plots
    a graph for every key with the values as x-axis values
    '''
    kvallist = list(range(2,21))
    for info in metrics:
        plt.xlabel("Kval")
        plt.ylabel(str(info))
        plt.title("%s over multiple Kvals" %(info))
        plt.plot(kvallist, metrics[info],c = 'r', label = str(info))
        plt.legend(loc = 2)
        plt.savefig('%s eval plot.png' %(info))
        plt.show()
    return

#################
###Functions to use
###In order
#################


def createandprint():
    #####
    #Creating and printing my ID3 implementation
    #####
    ID3tree = DecisionTree()
    ID3tree.build()
    ID3tree.printer()

 
def adder(a,b):
    print(a+b)


def classifyn(n=10):
    #####
    #Classifing 10 rows of a dataframe
    #on my ID3 tree
    #####
    ID3tree = DecisionTree()
    ID3tree.build()
    df = open_file_DF()
    maxrow = len(df)
    for i in range (0,n):
        row = randint(1,maxrow)
        prediction = ID3tree.classify(new_data = df, number = row)
        print('Prediction for row ',row, ': ' , prediction)
        print('Real value for row ',row, ': ' , df.iloc[row]['survived'])
    #####
    #Getting evaluation metrics from my ID3 tree
    #####
    print(evaluationmetrics(ID3tree, df))

    
def createsktree():
    #####
    #Creating sklearn decision tree
    #modifying a DataFrame to be able to be used
    #####
    df = open_file_DF()
    clf = DecisionTreeClassifier()
    df2, targets = encode_target(df, "survived")
    features = list(df2.columns[:3])
    y = df2["survived"]
    x = df2[features]
    clf.fit(x,y)
    #Exporting to dot file using export graphviz
    outf = open("dt.dot" , "w")
    export_graphviz(clf, out_file = outf, feature_names=features)

    #####
    #creating plots using skilearn evaluation metrics
    #####
    metrics = getavgmetrics(tree = clf,x = x,y= y,kval= 20)
    creategraphs(metrics)
    return


#createsktree()
