# -*- coding: utf-8 -*-
#Programmer: Christopher Young
#Class: Cpts215
#Assignment: Programming assignment 1
#Date:
#Description: 

from Scanner import *

       

def read_records(in_file):
    '''
    given a filename create a scanner that will
    create a record for each line
    return a list containing lines
    '''
    #create scanner to read from file
    tweet_scan = Scanner(in_file)
    #create a list to hold all the records from file
    return create_record(tweet_scan)
    #return records as a list
    #tweets will be in form:
    #[[handle,tweet,date],[handle,tweet,date], ... ]
#==============================================================================
#     tweet_list = [""]
#     while True:
#         tweet_list.insert(0,create_record(tweet_scan))
#         if tweet_list[0] == "":
#             break
#==============================================================================
    #return tweet_list
    

def create_record(in_scan):
    '''
    takes in a scanner and creates and returns a list or the record
    made from a tweet
    '''
    # empty list that will be filled with cleaned tweets
    # no @ or \n
    tweetlist = []
    line = in_scan.readline()
    while line != "": #stop at end of file
        #Clean the tweet
        #put stripped string into a list, then remove @
        #but back into string and append into list splitting on "
        mylist2 = list(line.strip())
        mylist2.remove("@")
        line = "".join(mylist2)
        tweetlist.append(line.split("\""))
        #tweet now made into a list
        #read next line
        line = in_scan.readline()
     #return list of tweets
    return (tweetlist)
#==============================================================================
#     
#     return line.split("\"")
#==============================================================================
    
def more_recent(tweet_1, tweet_2):
    '''
    compares 2 records based on date 
    true if first record is more recent
    '''
    #index 2 in each tweet is the date
    #compare date as numbers
    return( tweet_1[2] > tweet_2[2])#true if tweet 1 is more recent
       
    
def merge_and_sort_tweets(list_1, list_2):
    '''
    merges and sorts two lists of records based on date
    more recent records first
    '''
    #add list 1 and 2
    mylist = list_1+ list_2
    #sort based on index 2 (date) of each tweet within the list
    #reverse=true for most recent first
    #pass in mylist to lambda and return mylist[2] to be sorted by date
    mylist.sort(key = lambda mylist:mylist[2], reverse = True)
    #return merged and sorted list
    return mylist

def write_records(t_list,out_file):
    '''
    writes a list of records to a file
    one record per line
    '''
    #open file to right to from passed in file name string
    write_file = open(str(out_file), "w")
    #for every entry in the tweet list
    for entry in range(len(t_list)):
        #make the tweet into a string
        tweet = "\"".join(t_list[entry])
        #write tweet followed by new line
        write_file.write("%s \n" %(tweet))
    
    
def get_num_tweets(t_list):
    #length of list = number of tweets in list
    return len(t_list)

def avg_tweet_length(t_list):
    #setting up variables
    #total length of all tweets(to get avg)
    #how many are too long / too short
    total_length = 0
    too_long_count = 0
    short_count = 0
    #go through the tweets in the list
    for tweet in range(len(t_list)):
        #tweet is in index 1 and make it into a string
        tweet_word = "".join(t_list[tweet][1])
        #get length
        tweet_length = len(tweet_word)
        if(tweet_length > 150): #if it is longer than 150, it is too long
            too_long_count += 1
        elif(tweet_length < 50): #if it was under 50, it is too short
            short_count += 1
        total_length += tweet_length #sum up all the tweet lengths
    print ("%d tweets go over 150chars \n " %(too_long_count))
    print ("%d tweets go under 50chars \n " %(short_count))
    #average is total/ number of tweets
    average_length = total_length/len(t_list)
    print ("Average tweet length is: %.2f" %(average_length))
    return 0
    
def pause(key):
    '''
    pause cmd
    '''
    programpause = input("Press <%s> key to continue" %(key))
    
def main():
    '''
    Main Function
    '''
    File1 = input("File 1:")
    File2 = input("File 2:")
    #list & length from file 1
    t_list1 = read_records(File1)
    file1_length = len(t_list1)
    #list & length from file 2
    t_list2 = read_records(File2)
    file2_length = len(t_list2)
    #merging the tweet lists
    all_tweets = merge_and_sort_tweets(t_list1,t_list2)
    
    if file1_length > file2_length:
        print("%s has more tweets than %s with %d tweets \n" 
              %(File1,File2, file1_length))
    elif(file1_length < file2_length):
        print("%s has more tweets than %s with %d tweets \n" 
               %(File2,File1, file2_length))
    else:print("%s has as many tweets as %s with %d tweets \n" 
               %(File1,File2, file1_length))
            
    #print 5 most recent tweets from sorted list    
    print (more_recent(all_tweets[2], all_tweets[0]))
    for tweets in range(5):
        print("%s \n" %(all_tweets[tweets]))
    #print the number of tweets total
    print(get_num_tweets(all_tweets))
    #write to file
    write_records(all_tweets, "sorted_tweets.txt")
    avg_tweet_length(all_tweets)
    pause('any') #pause cmd
    print("done")
    pass
main()

