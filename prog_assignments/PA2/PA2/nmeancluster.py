# -*- coding: utf-8 -*-
"""
Created on Sat Sep 23 16:15:57 2017

@author: kicky
"""

import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.mlab as mlab
import numpy as np
import random

def cancerfinder(filename):
    cancerpd = pd.read_csv(r"cancer.csv", sep = ",", header = None, index_col = 0)
    
    print(cancerpd)
    
    plt.imshow(cancerpd, cmap = "seismic")
    plt.show()
    print("plot1")
    cancerpdsorted = n_means(cancerpd)
    print("plot2")
    print (cancerpdsorted)
    plt2 = plt.imshow(cancerpdsorted, cmap = "seismic")
#==============================================================================
#     plt.show()
#     
#     x = euclidian_dist(cancerpd.iloc[2], cancerpd.iloc[3])
#     print(cancerpd.max())
#     print(x)
#     print(getclusters(cancerpd))
#==============================================================================
    
def n_means(cancerpd):
    return split_data(cancerpd)
    
def split_data(cancerpd):
    '''splits all the data into 3 clusters,
    at first by random centroids and comparing distances to them from 
    points inside panda, then by choosing centroids at along the solution
    vectors of each cluster space and swapping vectors if they are closer 
    to another solution'''
    clusters = pd.DataFrame()
    clusters = clusters.append(getclusters(cancerpd))
    print(clusters)
    return clusters
    
#==============================================================================
#         print(xpd)
#         dist = euclidian_dist(xpd,cancerpd.iloc[0])
#         print("the distance between ^^ && index 0 is %.02f" %(dist))
#         x2 = dist
#         if x2 < x1:
#             cluster = y+1
#         x1 = dist
#         print ("cluster = %d" %(cluster))
#==============================================================================

def getclusters(cancerpd):
    centroids = createcentroids(cancerpd)
    print ("centroids: %s" %(centroids))
    #clusters = 3*[[0]]
    #Create 3 clusters
    cluster0 = pd.DataFrame()
    cluster1 = pd.DataFrame()
    cluster2 = pd.DataFrame()
    #compare the distance from each point and
    #places into closest cluster
    for x in range(0,cancerpd.shape[0]):
        #find distance form the points to each centroid respectivly
        dist_to0 = euclidian_dist(centroids[0], cancerpd.iloc[x])
        dist_to1 = euclidian_dist(centroids[1], cancerpd.iloc[x])
        dist_to2 = euclidian_dist(centroids[2], cancerpd.iloc[x])
        #Asks which centroid is closest
        #i.e. if dist to cantroid 0 is lowest, put inside the 0 cluster
        if (dist_to0<dist_to1 and dist_to0<dist_to2):
            cluster0 = cluster0.append(cancerpd.iloc[x])
        if (dist_to1<dist_to0 and dist_to1<dist_to2):
            cluster1 = cluster1.append(cancerpd.iloc[x])
        if (dist_to2<dist_to1 and dist_to2<dist_to0):
            cluster2 = cluster2.append(cancerpd.iloc[x])
    clusterlist = [cluster0, cluster1, cluster2]
    
    #clusterspd = pd.concat(clusterlist)
    clusterspd = pd.concat(createavgcentroids(clusterlist, cancerpd))
    return clusterspd
    #return clusters
    
def avgsort_clusters(pdlist):
    print()
    
def createavgcentroids(clusterlist, cancerpd):
    print("IN CREATEAVGCENTROIDS: \n\n")
    print("clusterlist:")
    print(clusterlist)
    average_0 = pandaaverage(clusterlist[0])
    average_1 = pandaaverage(clusterlist[1])
    average_2 = pandaaverage(clusterlist[2])
    loop = True
    while (loop == True):
        centroids = []
        temp = (cancerpd.shape[1])*[average_0]
        centroids.append(temp)
        temp = (cancerpd.shape[1])*[average_1]
        centroids.append(temp)
        temp = (cancerpd.shape[1])*[average_2]
        centroids.append(temp)
        print("CENTROIDSLIST: ")
        print(centroids)
        cluster0 = pd.DataFrame()
        cluster1 = pd.DataFrame()
        cluster2 = pd.DataFrame()
        for x in range(0,cancerpd.shape[0]):
            dist_to0 = euclidian_dist(centroids[0], cancerpd.iloc[x])
            dist_to1 = euclidian_dist(centroids[1], cancerpd.iloc[x])
            dist_to2 = euclidian_dist(centroids[2], cancerpd.iloc[x])
            if (dist_to0<dist_to1 and dist_to0<dist_to2):
                cluster0 = cluster0.append(cancerpd.iloc[x])
            if (dist_to1<dist_to0 and dist_to1<dist_to2):
                cluster1 = cluster1.append(cancerpd.iloc[x])
            if (dist_to2<dist_to1 and dist_to2<dist_to0):
                cluster2 = cluster2.append(cancerpd.iloc[x])
        average_0t = pandaaverage(cluster0)
        average_1t = pandaaverage(cluster1)
        average_2t = pandaaverage(cluster2)
        print("First 0 average: %.02f Second 0 average: %.02f" %(average_0,average_0t))
        print("First 1 average: %.02f Second 1 average: %.02f" %(average_1,average_1t))
        print("First 2 average: %.02f Second 2 average: %.02f" %(average_2,average_2t))
        if average_0t == 0 : average_0t = random.uniform(0,pandamax(cancerpd))
        if average_1t == 0 : average_1t = random.uniform(0,pandamax(cancerpd))
        if average_2t == 0 : average_2t = random.uniform(0,pandamax(cancerpd))
        if (average_0 == average_0t) and (average_1 == average_1t) and (average_2 == average_2t):
            loop = False
            print("loop is false")
        else:
            average_0 = average_0t
            average_1 = average_1t
            average_2 = average_2t
    print([cluster0,cluster1,cluster2])
    return [cluster0,cluster1,cluster2]


def createcentroids(cancerpd):
    '''Creates 3 random centroids inside of the
    same space basis as the points inside a panda
    using the lists inside a panda as points (X1,X2,...,Xn)'''
    #call pandamax function
    maxpd = pandamax(cancerpd)
    centroids = []
    for y in range(0,3):
        
        temppd = [random.uniform(0,maxpd)]
        for x in range(1, cancerpd.shape[1]):
            rnd = random.uniform(0,maxpd)
            temppd.append(rnd)
        centroids.append(temppd)
    return centroids
    
def pandamax(pnd):
    '''iterates through all rows of panda
    compares maxes of each row to find
    max of entire panda'''
    i = 0
    for x in range(0,pnd.shape[0]):
        t = pnd.iloc[x].max()
        if t > i: i=t
    return i

def pandaaverage(pnd):
    total = 0
    for x in range(0,pnd.shape[0]):
        total+=pnd.iloc[x].sum()
    x = pnd.shape[0]
    y = pnd.shape[1]
    if x==0: x+=1
    if y==0: y+=1
    return total/(x*y)

def euclidian_dist(a,b):
    '''squareroot of the sum of the differences of points squared'''
    return (sum((a-b)**2))**0.5

def main(a):
    cancerfinder("simple.csv")
    
main(5)