# -*- coding: utf-8 -*-
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.mlab as mlab
import numpy as np
import random

def cancerfinder():
    cancerpd = pd.read_csv(r"simple.csv", sep = ",", header = None, index_col = 0)
    #plt1 = plt.imshow(cancerpd, cmap = "seismic")
    cancerpd2 = pd.DataFrame(sort_mean(cancerpd))
    plt2 = plt.imshow(cancerpd2, cmap = "seismic")
    
def findmean(pnd):
    total = 0
    for x in range(0,pnd.shape[0]):
        total+=pnd.iloc[x].sum()
    x = pnd.shape[0]
    y = pnd.shape[1]
    if x==0: x=1
    if y==0: y=1
    return total/(x*y)
    
def euclidian_dist(a,b):
    '''squareroot of the sum of the differences of points squared'''
    return (sum((a-b)**2))**0.5

def pandamax(pnd):
    '''iterates through all rows of panda
    compares maxes of each row to find
    max of entire panda'''
    i = 0
    for x in range(0,pnd.shape[0]):
        t = pnd.iloc[x].max()
        if t > i: i=t
    return i

def sort_mean(pnd):
    '''returns a list of k data structures
    sorted by the mean value'''
    leftcluster = pd.DataFrame()
    rightcluster = pd.DataFrame()
    mu = findmean(pnd)
    #mu = pandamax(pnd)/2
    print(mu)
    firqu = pnd.shape[1] * [mu/2]
    print(firqu)
    secqu = pnd.shape[1] * [ (6*mu)/4 ]
    print(secqu)
    muvector = pnd.shape[1] * [mu]
    for i in range(0,pnd.shape[0]):
        #if  ((pnd.iloc[i] - muvector).sum()) < 0:
        if euclidian_dist(pnd.iloc[i], firqu) < euclidian_dist(pnd.iloc[i], secqu):
            leftcluster = leftcluster.append(pnd.iloc[i])
        else: rightcluster = rightcluster.append(pnd.iloc[i])
    #leftcluster.sort_values()
    #rightcluster.sort_values(int)
    print(leftcluster)
    print(rightcluster)
    clusters = pd.concat([leftcluster, rightcluster])
    return clusters
    
def main():
    cancerfinder()

main()
