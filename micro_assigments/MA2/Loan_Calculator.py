# -*- coding: utf-8 -*-
"""
Created on Fri Sep  8 14:14:27 2017

@author: kicky
"""

def monthly_pay(total,intres,length):
    intrest = (intres/100)/12
    return (intrest*total)/(1-(1+(intrest))**(-length))
    
def calc_prince(interest,remainder,payment):
    return (payment-(((interest/100)/12)*remainder))

def remaining_loan_bal(total,intrest,length):
    payment = float("{0:.02f}".format(monthly_pay(total,intrest,length)))
    remainderlist = []
    #remainder list in format:
        #[month, principle, interest, payment]
    for month in range(length+1):
        remainderlist.append([month,0,0,0,payment])
    remainderlist[0][1] = total
    for month in range(0,length):
        #a = principle for the month using calc_prince
        a = float("{0:.2f}".format(calc_prince(intrest,remainderlist[month][1],payment)))
        remainderlist[month][2] = a
        #b = interest by subtracting the princliple from the monthly payment
        b = float("{0:.2f}".format(remainderlist[month][4] - remainderlist[month][2]))
        remainderlist[month][3] = b
        #c = new loan ammount
        c = float("{0:.2f}".format(remainderlist[month][1]-remainderlist[month][2])) 
        remainderlist[month+1][1] = c
    remainderlist[length] = [length,0,0,0,0]
    return remainderlist

def write_records(t_list,out_file):
    '''
    writes a list of records to a file
    one record per line
    '''
    #open file to right to from passed in file name string
    write_file = open(str(out_file), "w")
    #for every entry in the tweet list
    for x in range(len(t_list)):
        for y in range(len(t_list[x])):
            write_file.write(" %.2f " %(t_list[x][y]))
        write_file.write("\n")
     

def main():
    loanfor = input("What is the loan is for:")
    totalbalance = float(input("Total of the loan:"))
    intrestrate = float(input("Intrest rate as a percent:"))
    lengthofloan = int(input("Length of loan in years:"))
    lengthofloan *= 12
    print("\n")
    x = remaining_loan_bal(totalbalance,intrestrate,lengthofloan)
    print(x)
    write_records(x,"%s_loan.txt" %(loanfor))
    
    
main()