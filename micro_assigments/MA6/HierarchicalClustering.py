# -*- coding: utf-8 -*-


import pandas as pd
import PrioHeap as ph

class Pair():
    def __init__(self, title = None, cluster1 = None, cluster2 = None,dist = 0):
        self.title = title
        self.distance = dist         
        self.cluster1 = cluster1
        self.cluster2 = cluster2
        self.centroid = self.computecentroid(self.cluster1, self.cluster2)
    
    def __str__(self):
        return ('%s' %(self.title))
        
    def computecentroid(self, cluster1, cluster2):
        '''
        creates a cluster from two dataframes to be used to later
        calculate distance bewteen itself and other data  points
        '''
        if type(cluster1) is Pair:
            return self.centroidbetweenpairs(cluster1, cluster2)
        elif cluster1 is not None:
            frames = [cluster1, cluster2]
            df = pd.concat(frames, axis = 0)
            return average_centroid(df)
    def getcentroid(self):
        return self.centroid
    def getdist(self):
        ''' returns distance '''
        return self.distance
    def centroidbetweenpairs(self,pair1,pair2):
        x = pair1.getcentroid()
        y = pair2.getcentroid()
        z = [(x[i]+y[i]) for i in range(len(x))]
        z = [z[i]/2 for i in range(len(z))]
        return z
        
        
    def __gt__(self, other):
        return self.getdist() > other
    def __lt__(self, other):
        '''
        returns true if the distance between values in pair
        is less than distance between values in other pair
        '''
        return self.getdist() < other
    
def average_centroid(df):
    return df.mean().tolist()

def open_file_DF(filename = None):
    '''
    opens a file(csv format) and returns a
    dataframe
    '''
    if filename is None:
        input_file = input("File to Open: ")
        try:
            with open("%s" %(input_file)) as a_file:
                dataframe = pd.read_csv(a_file, header = None, index_col = 0)
                return dataframe
        except IOError:
            print("file could not be open")
    else:
        try:
            with open("%s" %(filename)) as a_file:
                dataframe = pd.read_csv(a_file, header = None, index_col = 0)
                return dataframe
        except IOError:
            print("file could not be open")
   
class HeirarchicalClustering():
    def __init__(self):
        self.queue = []
    
    def build(self, df):
        pairs = self.getleastpairDF(df)
        self.getleastpair(pairs)
        #Create root from the first and second pairs in queue
        distance = euclidian_dist_list(self.queue[0].getcentroid(),self.queue[1].getcentroid())
        label = [(self.queue[0].title),(self.queue[1].title)]
        root = Pair(title = label, cluster1 = self.queue[0], cluster2 = self.queue[1], dist = distance)
        self.queue.insert(0, root)
        self.queue.insert(0, 0)
        
    def __str__(self):
        return str(self.queue)
    def printer(self):
        '''
        Prints tree using heap algorithm
        root = i
        left child = i*2
        right child = i*2 + 1
        '''
        for i in range(1,len(self.queue)//2):
            print('Node: ' , self.queue[i])
            print('leftchild: ' , self.queue[i*2])
            print('rightchild: ' , self.queue[(i*2)+1])
            
    def getleastpairDF(self, dfog):
        '''
        From a dataframe, pushes all leaf nodes onto heap
        returns a list of pairs to be used to 
        further create heap
        '''
        df = dfog.copy()    #create copy of df
        pairlist = []       #list for pairs to return
        while not df.empty:     #continue till df is empty
            least = None        #initialize least to None
            leastrow1 = 0           #no distances have been found
            leastrow2 = 0
            for i in range(0, len(df)):     #i for first pointer
                row1 = df.iloc[i]           #row1 set to df[i]
                for j in range(0, len(df)): #j for second pointer
                    if i !=  j:             #if at same row do nothing
                        row2 = df.iloc[j]       #row2 set to df[j]
                        dist = euclidian_dist(row1,row2)
                                            #distance between points
                        if [least is None or 
                            dist < least]:  #if no distance had been found yet
                                                #of new distance is less than least
                            least = dist    #update least
                            leastrow1 = i   #save location of least rows
                            leastrow2 = j
                            
            self.queue.append(Pair(title = df.index[leastrow1]))    #push leafs onto heap
            self.queue.append(Pair(title = df.index[leastrow2]))
                                        #push pair of closest leaves onto pairlist
            pairlist.append(Pair(title = [str(df.index[leastrow1]), str(df.index[leastrow2])],
                                          cluster1 = df.iloc[[leastrow1]], cluster2 = df.iloc[[leastrow2]],
                                          dist = least))
            df.drop(df.index[[leastrow1,leastrow2]], inplace = True) #drop closest rows
        return pairlist         #return a list of pairs
    
    def getleastpair(self, pairlistog):
        '''
        Finds the least distance between pairs
        inserts the children into heap,
        creates pair of two children and adds to 
        a list of pairs to make new pairs from
        '''
        if len(pairlistog) == 0: return     #when there is no more in the list
        pairlist = list(pairlistog)         #create deep copy of pairlist
        pairlistog = []                     #empty pairlist
        while(len(pairlist) > 1):           #while there is more than one
                                                #item to make pairs from
            least = None                    #initialize least distance to none
            leastrow1 = 0                       #(no distance found yet)
            leastrow2 = 0
            for i in range(0,len(pairlist)):    #i for first pointer
                row1 = pairlist[i]              #row 1 set to pairlist[i]
                for j in range(0,len(pairlist)):    #j for second pointer
                    if i != j:                  #if both are at the same spot
                                                    #do nothing
                        row2 = pairlist[j]      #row2 set to pointerlist[j]
                        distance = euclidian_dist_list(row1.getcentroid(),row2.getcentroid())
                                                #distance between pairs
                        if [least is None or 
                            distance < least]:  #if no distance had been found yet
                                                #of new distance is less than least
                            least = distance    #update least
                            leastrow1 = i       #save location of least rows
                            leastrow2 = j
            self.queue.insert(0, pairlist[leastrow1])   #push both children onto heap
            self.queue.insert(0, pairlist[leastrow2])
            label = [(pairlist[leastrow1].title),(pairlist[leastrow2].title)]
                                                #create the pair and add to pairlist
            pairlistog.append(Pair(title = label,cluster1 = pairlist[leastrow1],cluster2 = pairlist[leastrow2], dist = least))
            del pairlist[leastrow1]             #delete rows we made pairs from
            del pairlist[leastrow2]
        if len(pairlist) == 1:              #if only 1 item left in list
            self.queue.insert(0, pairlist[0])   #push last child onto heap
            del pairlist[0]                 #delete pushed child
        self.getleastpair(pairlistog)       #reccur on list of pairs
    
def euclidian_dist(a,b):
    return sum((a-b)**2)**0.5
    
    
def euclidian_dist_list(a,b):
    '''squareroot of the sum of the differences of points squared'''
    z = [(a[i]-b[i])**2 for i in range(len(a))]
    return (sum(z))**0.5     
    
def making_a_tree():
    df = open_file_DF('simple.csv')
    hc = HeirarchicalClustering()
    hc.build(df)
    hc.printer()

