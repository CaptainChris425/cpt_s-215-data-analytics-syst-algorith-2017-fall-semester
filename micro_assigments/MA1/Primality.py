# -*- coding: utf-8 -*-
##########################################
#Programmer:Christopher Young
#Class:CptS215
#Assignment:MA1 Primality
#Date:
#Description:
#    Create an algorithm to detemine if an imputed value is prime or composite
#    -Imput number x
#    -Check from 2->sqrt(x) if number is divisible without remainder
#    -return t/f to validate primality
##########################################
 
#Checks an inputted value to see if it is prime
def is_prime(number):
    '''
    This function checks if a number is prime by dividing by each number
    up to the square root of the number to check if dividing will create a remainder
    or not
    '''
    prime_check = True
#==============================================================================
#     end = int(number**.5)
#     for i in range(1,end):
#         if number%i == 0:
#             prime_check = False
#==============================================================================
#==============================================================================
    counter=2 
    while (counter <= (number**.5)):
        #if dividing by any number leaves 0 remainder the number is composite
        if number%counter == 0: prime_check = False
        counter += 1
    #True for prime, false for composite
    return prime_check
#Sums up all prime numbers up to an inputted stopping point
def sum_prime(stopnum):
    '''
    This function sums up all the prime numbers leading up to a
    (and including if prime)
    parameter specified stopping value and returns the sum
    '''
    count = 2
    total = 0
    while (count <= stopnum):
        if (is_prime(count)): total+=count
        count += 1
    print("Sum of prime numbers up to" , stopnum , "is" , total)
    return total

def invalid_number(n):
    '''
    Checks to see if input is a valid integer value.
        ie. no decimals or negatives
    '''
    #checking if number is an integer by comparing its int value to its true value
    if (int(n) != n): 
        n=1
        print("\nThat was not an integer, are you trying to trick me?\n")
    #if it matches values and is greater than or is 2 it is NOT an invalid number
    if (n>=2): return False
    else: return True
    

''' Get imput from user value to check for primility'''
def main():
    #Getting the number to check
    n=float((input("If you tell me a number, I will tell you if it is prime. \n" 
          "Enter an integer with value 2 or larger:")))
    #checking until valid number is entered
    while (invalid_number(n)):
        n=float((input("Enter an integer with value 2 or larger:")))
    #calling prime check function
    if (is_prime(n)):   print ("The number" , n , "is prime")
    else:   print("The number" , n , "is composite")
    
    #sum up prime numbers up to inputted m value
    print("\nI can also sum up all the prime numbers up to a number ")
    m=float((input("Enter any integer value:")))
    #checking until valid number is entered
    while (invalid_number(m)):
        m=float((input("Enter an integer with value 2 or larger: ")))
    #calling sum prime function to sum the leading prime numbers
    sum_prime(m)
    return 0

main()



